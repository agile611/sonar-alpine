# Sonar Alpine

# Introduction

Build the application using the following command
```
docker build -t sonar-alpine:latest .
```

Run the application using the following command
```
docker run -p 9000:9000 -p 9092:9092 sonar-alpine:latest
```

Run the application using the following command

```
http://localhost:9000/
```

## License and Support
### Apache 2.0 License
[![License](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg)](https://opensource.org/licenses/Apache-2.0)  

This code is released into the public domain by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/) under [![License](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg)](https://opensource.org/licenses/Apache-2.0)  

This README file was originally written by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/) and is likewise released into the public domain.

Please contact [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/) for further details.
